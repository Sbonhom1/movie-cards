
// Ajax Request
var ourRequest = new XMLHttpRequest();
//Points to Template Div
var template = document.getElementById("app");

// 'GET' to receive data or 'POST' to send data
ourRequest.open('GET','https://api.themoviedb.org/3/movie/now_playing?api_key=41497481ed2a0b80f9e2a7907402796e&language=en-US');
ourRequest.onload = function() {

  // Movies Array
  var movieData = JSON.parse(ourRequest.responseText);

  // Pass movie data to the function
  renderTemplate(movieData);

  console.log(movieData);

  // Run Adaptive Background Color
  $.adaptiveBackground.run({
      normalizeTextColor: true
    });

};

ourRequest.send();

// Function to render HTML tot he page
function renderTemplate(data) {

  var rawTemplate = document.getElementById("slider-template").innerHTML;

   var compiledTemplate = Handlebars.compile(rawTemplate);

   var ourGeneratedSlide = compiledTemplate(data);

   var template = document.getElementById("app");

   template.innerHTML = ourGeneratedSlide;
};
